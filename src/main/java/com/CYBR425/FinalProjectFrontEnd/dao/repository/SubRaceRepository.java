package com.CYBR425.FinalProjectFrontEnd.dao.repository;

import com.CYBR425.FinalProjectFrontEnd.dao.SubRace;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubRaceRepository extends JpaRepository<SubRace, Integer> {

    List<SubRace> findAll();

}
