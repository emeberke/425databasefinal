package com.CYBR425.FinalProjectFrontEnd.dao.repository;

import com.CYBR425.FinalProjectFrontEnd.dao.DndSpells;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SpellRepository extends JpaRepository<DndSpells, Integer> {

    List<DndSpells> findAll();

    List<DndSpells> findAllById(int id);

}
