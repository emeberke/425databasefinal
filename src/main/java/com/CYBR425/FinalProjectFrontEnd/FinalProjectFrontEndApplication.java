package com.CYBR425.FinalProjectFrontEnd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalProjectFrontEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalProjectFrontEndApplication.class, args);
	}

}
