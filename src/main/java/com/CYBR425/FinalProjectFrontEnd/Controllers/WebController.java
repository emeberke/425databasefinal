package com.CYBR425.FinalProjectFrontEnd.Controllers;

import com.CYBR425.FinalProjectFrontEnd.dao.*;
import com.CYBR425.FinalProjectFrontEnd.service.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/")
public class WebController {

    @Autowired
    private SearchController searchController;

    @Autowired
    private CharacterService characterService;

    @GetMapping(value="home")
    public String findCharacters(ModelMap modelMap) {
        List<DndCharacter> characters = searchController.showDndCharacters();
        List<DndClass> classes = searchController.showDndClasses();
        List<DndRace> races = searchController.showDndRaces();
        List<DndSpells> spells = searchController.showDndSpells();
        List<SubRace> subRaces = searchController.showSubraces();

        modelMap.put("characters", characters);
        modelMap.put("classes", classes);
        modelMap.put("races", races);
        modelMap.put("spells", spells);
        modelMap.put("subraces", subRaces);
        return "index";
    }

    @GetMapping("search")
    public String search(ModelMap modelMap, @RequestParam(required = false) String nameText, @RequestParam(required = false) int raceId, @RequestParam(required = false) int classId,
                         @RequestParam(required = false) Integer nameId, @RequestParam(required = false) int spellId) {
        List<DndCharacter> characters = searchController.searchById(raceId, classId, nameId);
        List<DndSpells> spells = searchController.showDndSpells();
        List<DndRace> races = searchController.showDndRaces();
        List<DndClass> classes = searchController.showDndClasses();

        if(!nameText.isEmpty()) {
            characters = characterService.findAllByNameLike(nameText);
        }
        if(characters.isEmpty()) {
            DndCharacter noneExist = new DndCharacter();
            noneExist.setCharacterName("N/A");
            characters.add(noneExist);
        }
        if(spellId != 0) {
            spells = searchController.findSpellById(spellId);
        }
        modelMap.put("characters", characters);
        modelMap.put("races", races);
        modelMap.put("classes", classes);
        modelMap.put("spells", spells);
        return "result";
    }

    @GetMapping("search/add")
    public String addCharacterForm(ModelMap modelMap) {
        DndCharacter addCharacter = new DndCharacter();
        List<DndRace> races = searchController.showDndRaces();
        List<DndClass> classes = searchController.showDndClasses();

        modelMap.put("addCharacter", addCharacter);
        modelMap.put("races", races);
        modelMap.put("classes", classes);
        return "characterForm";
    }

    @PostMapping(value = "search/add")
    public String addCharacter(@ModelAttribute DndCharacter dndCharacter) {
        DndCharacter addedCharacter = new DndCharacter();
        List<DndCharacter> allCharacters = searchController.showDndCharacters();

        addedCharacter.setCharacterId(allCharacters.size() + 1);
        addedCharacter.setBackground(dndCharacter.getBackground());
        addedCharacter.setCharacterClass(dndCharacter.getCharacterClass());
        addedCharacter.setCharacterClassLevel(1);
        addedCharacter.setCharacterDescription(dndCharacter.getCharacterDescription());
        addedCharacter.setCharacterHealth(dndCharacter.getCharacterHealth());
        addedCharacter.setCharacterName(dndCharacter.getCharacterName());
        addedCharacter.setCharisma(dndCharacter.getCharisma());
        addedCharacter.setConstitution(dndCharacter.getConstitution());
        addedCharacter.setDexterity(dndCharacter.getDexterity());
        addedCharacter.setIntelligence(dndCharacter.getIntelligence());
        addedCharacter.setStrength(dndCharacter.getStrength());
        addedCharacter.setWisdom(dndCharacter.getWisdom());
        addedCharacter.setRace(dndCharacter.getRace());
        addedCharacter.setPictureUrl(dndCharacter.getPictureUrl());

        characterService.addCharacter(addedCharacter);
        return "redirect:/home";
    }

    @GetMapping("search/edit")
    public String editCharacterForm(ModelMap modelMap, @RequestParam(required = false) String action, @RequestParam(defaultValue="0") Integer characterId) {
        List<DndRace> races = searchController.showDndRaces();
        List<DndClass> classes = searchController.showDndClasses();
        DndCharacter toEdit = characterService.findCharacterById(characterId);

        if(action.equals("back")) {
            return "redirect:/home";
        }
        if(action.equals("add")) {
            return "redirect:/search/add";
        }
        if(action.equals("delete")) {
            characterService.deleteCharacter(characterId);
            return "redirect:/home";
        }

        modelMap.put("editCharacter", toEdit);
        modelMap.put("races", races);
        modelMap.put("classes", classes);
        return "characterEdit";
    }

    @PutMapping(value = "search/edit")
    public String editCharacter(@ModelAttribute DndCharacter updatedCharacter) {
        DndCharacter character = characterService.findCharacterById(updatedCharacter.getCharacterId());

        character.setBackground(updatedCharacter.getBackground());
        character.setCharacterClass(updatedCharacter.getCharacterClass());
        character.setCharacterClassLevel(1);
        character.setCharacterDescription(updatedCharacter.getCharacterDescription());
        character.setCharacterHealth(updatedCharacter.getCharacterHealth());
        character.setCharacterName(updatedCharacter.getCharacterName());
        character.setCharisma(updatedCharacter.getCharisma());
        character.setConstitution(updatedCharacter.getConstitution());
        character.setDexterity(updatedCharacter.getDexterity());
        character.setIntelligence(updatedCharacter.getIntelligence());
        character.setStrength(updatedCharacter.getStrength());
        character.setWisdom(updatedCharacter.getWisdom());
        character.setRace(updatedCharacter.getRace());
        character.setPictureUrl(updatedCharacter.getPictureUrl());
        characterService.updateCharacter(character);

        return "redirect:/home";
    }
}
