package com.CYBR425.FinalProjectFrontEnd.service;

import com.CYBR425.FinalProjectFrontEnd.dao.DndCharacter;
import com.CYBR425.FinalProjectFrontEnd.dao.repository.DndCharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CharacterService {

    @Autowired
    DndCharacterRepository dndCharacterRepository;

    public DndCharacter addCharacter(DndCharacter dndCharacter) {
        return dndCharacterRepository.save(dndCharacter);
    }

    public DndCharacter updateCharacter(DndCharacter dndCharacter) {
        return dndCharacterRepository.save(dndCharacter);
    }

    public List<DndCharacter> findAllByNameLike(String name) {
        return dndCharacterRepository.findAllByCharacterNameContaining(name);
    }

    public void deleteCharacter(int id) {
        dndCharacterRepository.deleteDndCharacterByCharacterId(id);
    }

    public DndCharacter findCharacterById(int characterId) {
        return dndCharacterRepository.findDndCharacterByCharacterId(characterId);
    }
}
