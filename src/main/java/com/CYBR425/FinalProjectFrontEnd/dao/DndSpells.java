package com.CYBR425.FinalProjectFrontEnd.dao;

import javax.persistence.*;

@Entity
@Table(name = "DND_SPELLS")
public class DndSpells {

    @Id
    @Column(name = "SPELL_ID")
    private int id;

    @Column(name = "SPELL_NAME")
    private String spellName;

    @Column(name = "SPELL_LEVEL")
    private int spellLevel;

    @Column(name = "SCHOOL_OF_MAGIC")
    private String schoolOfMagic;

    @Column(name = "RITUAL_FLAG")
    private boolean ritual;

    @Column(name = "CASTING_TIME")
    private int castingTime;

    @Column(name = "SPELL_RANGE")
    private int spellRange;

    @Column(name = "COMPONENTS")
    private String components;

    @Column(name = "CONCENTRATION_FLAG")
    private boolean concentration;

    @Column(name = "DURATION")
    private int duration;

    @Column(name = "PICTURE_URL")
    private String pictureUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSpellName() {
        return spellName;
    }

    public void setSpellName(String spellName) {
        this.spellName = spellName;
    }

    public int getSpellLevel() {
        return spellLevel;
    }

    public void setSpellLevel(int spellLevel) {
        this.spellLevel = spellLevel;
    }

    public String getSchoolOfMagic() {
        return schoolOfMagic;
    }

    public void setSchoolOfMagic(String schoolOfMagic) {
        this.schoolOfMagic = schoolOfMagic;
    }

    public boolean isRitual() {
        return ritual;
    }

    public void setRitual(boolean ritual) {
        this.ritual = ritual;
    }

    public int getCastingTime() {
        return castingTime;
    }

    public void setCastingTime(int castingTime) {
        this.castingTime = castingTime;
    }

    public int getSpellRange() {
        return spellRange;
    }

    public void setSpellRange(int spellRange) {
        this.spellRange = spellRange;
    }

    public String getComponents() {
        return components;
    }

    public void setComponents(String components) {
        this.components = components;
    }

    public boolean isConcentration() {
        return concentration;
    }

    public void setConcentration(boolean concentration) {
        this.concentration = concentration;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
