package com.CYBR425.FinalProjectFrontEnd.Controllers;

import com.CYBR425.FinalProjectFrontEnd.dao.*;
import com.CYBR425.FinalProjectFrontEnd.service.CharacterService;
import com.CYBR425.FinalProjectFrontEnd.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/search/")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @Autowired
    private CharacterService characterService;

    @GetMapping("characters")
    @ResponseBody
    public List<DndCharacter> showDndCharacters() {
        return searchService.findAllCharacters();
    }

    @GetMapping("classes")
    @ResponseBody
    public List<DndClass> showDndClasses() {
        return searchService.findAllClasses();
    }

    @GetMapping("races")
    @ResponseBody
    public List<DndRace> showDndRaces() {
        return searchService.findAllRaces();
    }

    @GetMapping("spells")
    @ResponseBody
    public List<DndSpells> showDndSpells() {
        return searchService.findAllSpells();
    }

    @GetMapping("subraces")
    @ResponseBody
    public List<SubRace> showSubraces() {
        return searchService.findAllSubraces();
    }

    @GetMapping("/characters/{nameId}")
    @ResponseBody
    public List<DndCharacter> findCharactersById(@PathVariable int nameId) {
        return searchService.findCharactersById(nameId);
    }

    @GetMapping("/classes/{classId}")
    @ResponseBody
    public DndClass findClassById(@PathVariable int classId) {
        return searchService.findClassById(classId);
    }

    @GetMapping("/races/{raceId}")
    @ResponseBody
    public DndRace findRaceById(@PathVariable int raceId) {
        return searchService.findRaceById(raceId);
    }

    @GetMapping("/spells/{spellId}")
    @ResponseBody
    public List<DndSpells> findSpellById(@PathVariable int spellId) {
        return searchService.findSpellById(spellId);
    }

    @GetMapping("{name}")
    @ResponseBody
    public List<DndCharacter> findCharacterByName(@PathVariable String name) {
        return searchService.findAllByName(name);
    }

    @GetMapping("race/{raceId}/class/{classId}/name/{name}")
    @ResponseBody
    public List<DndCharacter> searchById(@PathVariable int raceId, @PathVariable int classId, @PathVariable Integer name) {
        List<DndCharacter> characters = null;

        if(raceId == 0 && classId == 0 && name == null) {
            characters = showDndCharacters();
        }
        if(raceId != 0 && classId == 0 && name == null) {
            characters = searchService.findAllByRace(raceId);
        }
        if(raceId == 0 && classId != 0 && name == null) {
            characters = searchService.findAllByClass(classId);
        }
        if(raceId == 0 && classId == 0 && name != null) {
            characters = searchService.findCharactersById(name);
        }
        if(raceId != 0 && classId != 0 && name == null) {
            characters = searchService.findAllByClassAndRace(classId, raceId);
        }
        return characters;
    }

}
