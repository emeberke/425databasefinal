package com.CYBR425.FinalProjectFrontEnd.dao;

import javax.persistence.*;

@Entity
@Table(name = "DND_SUBRACE")
public class SubRace {

    @Id
    @Column(name = "SUBRACE_ID")
    private int id;

    @Column(name = "SUBRACE")
    private String subrace;

    @ManyToOne
    @JoinColumn(name = "RACE")
    private DndRace race;

    @Column(name = "ABILITY_SCORE_CHANGES")
    private String subRaceASI;

    @Column(name = "SUBRACE_TRAITS")
    private String subRaceTraits;

    public int getId() {
        return id;
    }

    public String getSubrace() {
        return subrace;
    }

    public DndRace getRace() {
        return race;
    }

    public String getSubRaceASI() {
        return subRaceASI;
    }

    public String getSubRaceTraits() {
        return subRaceTraits;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSubrace(String subrace) {
        this.subrace = subrace;
    }

    public void setRace(DndRace race) {
        this.race = race;
    }

    public void setSubRaceASI(String subRaceASI) {
        this.subRaceASI = subRaceASI;
    }

    public void setSubRaceTraits(String subRaceTraits) {
        this.subRaceTraits = subRaceTraits;
    }
}
