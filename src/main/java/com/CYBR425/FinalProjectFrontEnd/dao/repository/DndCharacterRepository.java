package com.CYBR425.FinalProjectFrontEnd.dao.repository;

import com.CYBR425.FinalProjectFrontEnd.dao.DndCharacter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface DndCharacterRepository extends JpaRepository<DndCharacter, Integer> {

    List<DndCharacter> findAll();

    List<DndCharacter> findAllByCharacterId(int id);

    List<DndCharacter> findAllByCharacterClass(int classId);

    List<DndCharacter> findAllByRace(int raceId);

    List<DndCharacter> findAllByCharacterName(String name);

    List<DndCharacter> findAllByCharacterClassAndRace(int classId, int raceId);

    @Modifying
    @Query(value = "DELETE FROM DndCharacter c WHERE c.characterId = :id")
    int deleteDndCharacterByCharacterId(@Param("id") int id);

    List<DndCharacter> findAllByCharacterNameContaining(String name);

    DndCharacter findDndCharacterByCharacterId(int id);

}
