package com.CYBR425.FinalProjectFrontEnd.dao.repository;

import com.CYBR425.FinalProjectFrontEnd.dao.DndRace;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RaceRepository extends JpaRepository<DndRace, Integer> {

    List<DndRace> findAll();

    DndRace findById(int id);
}
