package com.CYBR425.FinalProjectFrontEnd.dao.repository;

import com.CYBR425.FinalProjectFrontEnd.dao.DndClass;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClassRepository extends JpaRepository<DndClass, Integer> {

    List<DndClass> findAll();

    DndClass findById(int id);

}
