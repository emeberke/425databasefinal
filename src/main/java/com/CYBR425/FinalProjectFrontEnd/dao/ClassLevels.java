package com.CYBR425.FinalProjectFrontEnd.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DND_CLASS_LEVELS")
public class ClassLevels {

    @Id
    @Column(name = "CLASS_ID")
    private int classID;

    @Column(name = "CLASS_LEVEL")
    private int classLevel;

    @Column(name = "PROFICIENCY_BONUS")
    private int profBonus;

    @Column(name = "CLASS_FEATURES")
    private String classFeature;

    @Column(name = "CLASS_RESOURCE_NAME")
    private String resourceName;

    @Column(name = "CLASS_RESOURCE_USES")
    private int resourceUses;

    @Column(name = "SPELLS_KNOWN")
    private int spellsKnown;

    @Column(name = "SPELL_SLOTS")
    private int spellSlots;

    @Column(name = "CANTRIPS")
    private int cantrips;

    public int getClassID() {
        return classID;
    }

    public int getClassLevel() {
        return classLevel;
    }

    public int getProfBonus() {
        return profBonus;
    }

    public String getClassFeature() {
        return classFeature;
    }

    public String getResourceName() {
        return resourceName;
    }

    public int getResourceUses() {
        return resourceUses;
    }

    public int getSpellsKnown() {
        return spellsKnown;
    }

    public int getSpellSlots() {
        return spellSlots;
    }

    public int getCantrips() {
        return cantrips;
    }

    public void setClassID(int classID) {
        this.classID = classID;
    }

    public void setClassLevel(int classLevel) {
        this.classLevel = classLevel;
    }

    public void setProfBonus(int profBonus) {
        this.profBonus = profBonus;
    }

    public void setClassFeature(String classFeature) {
        this.classFeature = classFeature;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public void setResourceUses(int resourceUses) {
        this.resourceUses = resourceUses;
    }

    public void setSpellsKnown(int spellsKnown) {
        this.spellsKnown = spellsKnown;
    }

    public void setSpellSlots(int spellSlots) {
        this.spellSlots = spellSlots;
    }

    public void setCantrips(int cantrips) {
        this.cantrips = cantrips;
    }
}
