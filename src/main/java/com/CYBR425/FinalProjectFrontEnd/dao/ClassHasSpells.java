package com.CYBR425.FinalProjectFrontEnd.dao;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CLASS_HAS_SPELLS")
public class ClassHasSpells implements Serializable {

    @Id
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CLASS_ID")
    private DndClass dndClassID;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SPELL_ID")
    private DndSpells dndSpellID;

    public DndClass getDndClassID() {
        return dndClassID;
    }

    public DndSpells getDndSpellID() {
        return dndSpellID;
    }

    public void setDndClassID(DndClass dndClassID) {
        this.dndClassID = dndClassID;
    }

    public void setDndSpellID(DndSpells dndSpellID) {
        this.dndSpellID = dndSpellID;
    }
}
