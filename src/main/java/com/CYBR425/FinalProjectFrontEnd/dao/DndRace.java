package com.CYBR425.FinalProjectFrontEnd.dao;

import javax.persistence.*;

@Entity
@Table(name = "DND_RACE")
public class DndRace {

    @Id
    @Column(name = "RACE_ID")
    private int id;

    @Column(name = "RACE")
    private String race;

    @Column(name = "ABILITY_SCORE_CHANGES")
    private String racialAbilityScores;

    @Column(name = "RACIAL_TRAITS")
    private String racialTraits;

    @Column(name = "DARKVISION")
    private boolean darkvision;

    @Column(name = "LANGUAGE")
    private String racialLanguage;

    @Column(name = "SPEED")
    private int racialSpeed;

    @Column(name = "HAS_SUBRACE")
    private boolean hasSubrace;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getRacialAbilityScores() {
        return racialAbilityScores;
    }

    public void setRacialAbilityScores(String racialAbilityScores) {
        this.racialAbilityScores = racialAbilityScores;
    }

    public String getRacialTraits() {
        return racialTraits;
    }

    public void setRacialTraits(String racialTraits) {
        this.racialTraits = racialTraits;
    }

    public boolean hasDarkvision() {
        return darkvision;
    }

    public void setDarkvision(boolean darkvision) {
        this.darkvision = darkvision;
    }

    public String getRacialLanguage() {
        return racialLanguage;
    }

    public void setRacialLanguage(String racialLanguage) {
        this.racialLanguage = racialLanguage;
    }

    public int getRacialSpeed() {
        return racialSpeed;
    }

    public void setRacialSpeed(int racialSpeed) {
        this.racialSpeed = racialSpeed;
    }

    public boolean hasSubrace() {
        return hasSubrace;
    }

    public void setHasSubrace(boolean hasSubrace) {
        this.hasSubrace = hasSubrace;
    }
}
