package com.CYBR425.FinalProjectFrontEnd.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DND_CHARACTER")
public class DndCharacter {

    @Id
    @Column(name = "CHARACTER_ID")
    private Integer characterId;

    @Column(name = "BACKGROUND")
    private String background;

    @Column(name = "CHARACTER_CLASS")
    private Integer characterClass;

    @Column(name = "CHARACTER_CLASS_LEVEL")
    private Integer characterClassLevel;

    @Column(name = "CHARACTER_DESCRIPTION")
    private String characterDescription;

    @Column(name = "CHARACTER_HEALTH")
    private Integer characterHealth;

    @Column(name = "CHARACTER_NAME")
    private String characterName;

    @Column(name = "CHARISMA")
    private Integer charisma;

    @Column(name = "CONSTITUTION")
    private Integer constitution;

    @Column(name = "DEXTERITY")
    private Integer dexterity;

    @Column(name = "INTELLIGENCE")
    private Integer intelligence;

    @Column(name = "STRENGTH")
    private Integer strength;

    @Column(name = "WISDOM")
    private Integer wisdom;

    @Column(name = "RACE")
    private Integer race;

    @Column(name = "PICTURE_URL")
    private String pictureUrl;

    public Integer getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Integer characterId) {
        this.characterId = characterId;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public Integer getCharacterClass() {
        return characterClass;
    }

    public void setCharacterClass(Integer characterClass) {
        this.characterClass = characterClass;
    }

    public Integer getCharacterClassLevel() {
        return characterClassLevel;
    }

    public void setCharacterClassLevel(Integer characterClassLevel) {
        this.characterClassLevel = characterClassLevel;
    }

    public String getCharacterDescription() {
        return characterDescription;
    }

    public void setCharacterDescription(String characterDescription) {
        this.characterDescription = characterDescription;
    }

    public Integer getCharacterHealth() {
        return characterHealth;
    }

    public void setCharacterHealth(Integer characterHealth) {
        this.characterHealth = characterHealth;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public Integer getCharisma() {
        return charisma;
    }

    public void setCharisma(Integer charisma) {
        this.charisma = charisma;
    }

    public Integer getConstitution() {
        return constitution;
    }

    public void setConstitution(Integer constitution) {
        this.constitution = constitution;
    }

    public Integer getDexterity() {
        return dexterity;
    }

    public void setDexterity(Integer dexterity) {
        this.dexterity = dexterity;
    }

    public Integer getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(Integer intelligence) {
        this.intelligence = intelligence;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getWisdom() {
        return wisdom;
    }

    public void setWisdom(Integer wisdom) {
        this.wisdom = wisdom;
    }

    public Integer getRace() {
        return race;
    }

    public void setRace(Integer race) {
        this.race = race;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
