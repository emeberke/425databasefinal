package com.CYBR425.FinalProjectFrontEnd.dao;

import javax.persistence.*;

@Entity
@Table(name = "DND_CLASS")
public class DndClass {

    @Id
    @Column(name = "CLASS_ID")
    private int id;

    @Column(name = "CLASS")
    private String className;

    @ManyToOne
    @JoinColumn(name = "CLASS_LEVEL")
    private ClassLevels classLevel;

    @Column(name = "HIT_DIE")
    private int hitDie;

    @Column(name = "ARMOR_PROFICIENCIES")
    private String armorProficiencies;

    @Column(name = "WEAPON_PROFICIENCIES")
    private String weaponProficiencies;

    @Column(name = "SAVING_THROWS")
    private String savingThrows;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public ClassLevels getClassLevel() {
        return classLevel;
    }

    public void setClassLevel(ClassLevels classLevel) {
        this.classLevel = classLevel;
    }

    public int getHitDie() {
        return hitDie;
    }

    public void setHitDie(int hitDie) {
        this.hitDie = hitDie;
    }

    public String getArmorProficiencies() {
        return armorProficiencies;
    }

    public void setArmorProficiencies(String armorProficiencies) {
        this.armorProficiencies = armorProficiencies;
    }

    public String getWeaponProficiencies() {
        return weaponProficiencies;
    }

    public void setWeaponProficiencies(String weaponProficiencies) {
        this.weaponProficiencies = weaponProficiencies;
    }

    public String getSavingThrows() {
        return savingThrows;
    }

    public void setSavingThrows(String savingThrows) {
        this.savingThrows = savingThrows;
    }
}
