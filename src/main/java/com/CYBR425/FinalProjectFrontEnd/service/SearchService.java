package com.CYBR425.FinalProjectFrontEnd.service;

import com.CYBR425.FinalProjectFrontEnd.dao.*;
import com.CYBR425.FinalProjectFrontEnd.dao.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SearchService {

    @Autowired
    DndCharacterRepository dndCharacterRepository;

    @Autowired
    ClassRepository classRepository;

    @Autowired
    RaceRepository raceRepository;

    @Autowired
    SpellRepository spellRepository;

    @Autowired
    SubRaceRepository subRaceRepository;

    public List<DndCharacter> findAllCharacters() {
        return this.dndCharacterRepository.findAll();
    }

    public List<DndClass> findAllClasses() {
        return this.classRepository.findAll();
    }

    public List<DndRace> findAllRaces() {
        return this.raceRepository.findAll();
    }

    public List<DndSpells> findAllSpells() {
        return this.spellRepository.findAll();
    }

    public List<SubRace> findAllSubraces() {
        return this.subRaceRepository.findAll();
    }

    public List<DndCharacter> findCharactersById(int id) {
        return this.dndCharacterRepository.findAllByCharacterId(id);
    }

    public DndClass findClassById(int id) {
        return this.classRepository.findById(id);
    }

    public DndRace findRaceById(int id) {
        return this.raceRepository.findById(id);
    }

    public List<DndSpells> findSpellById(int id) {
        return this.spellRepository.findAllById(id);
    }

    public List<DndCharacter> findAllByClass(int classId) {
        return this.dndCharacterRepository.findAllByCharacterClass(classId);
    }

    public List<DndCharacter> findAllByRace(int raceId) {
        return this.dndCharacterRepository.findAllByRace(raceId);
    }

    public List<DndCharacter> findAllByName(String name) {
        return this.dndCharacterRepository.findAllByCharacterName(name);
    }

    public List<DndCharacter> findAllByClassAndRace(int classId, int raceId) {
        return this.dndCharacterRepository.findAllByCharacterClassAndRace(classId, raceId);
    }

}
